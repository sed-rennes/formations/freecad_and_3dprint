# step 0 : Define your part

If you want to print a replacement part, you need to get from this :
* dimensions characteristics and associates tolerances,
* mechanical constraints (how the part work)
* environmental constraints ( extreme temperatures, chemical agents in contact, UV lights, etc.)
* Usage responsibility :
    * this part can support life?
    * is critical part of a system ?
    * can be used is food manipulation ?
    * has this part an environmental impact ?
    * if this part malfunctions what damages you must insure ?
    * Is this part works well, what kind of moral consequences will happen ?
    * etc.
* etc.

All this aspect can define constraints for you to redesign or just copy your part and choice a technologie for do that.

Then after all this considerations, next, imagine for this tutorial a very simple part (a peg) used in this very sophisticated system : ![pressa image example](https://www.ikea.com/gb/en/images/products/pressa-hanging-dryer-16-clothes-pegs-turquoise__0662635_PE712056_S5.JPG?f=g)

Suppose some pegs are broken and you don't know how to buy a replacement set. 
You can use a photocopier or 2D scanner to produce this kind of images of your broken part :

![broken peg front scan](images/scan_front_peg.jpeg)

![broken peg side scan](images/scan_side_peg.jpeg)

You can see that the original par is broken in two by mechanical fatigue on deformation of a sort of hinge joint of the peg.

Let's go to see how to integrate this images in FreeCAD to help us to define a replacement peg. 

# [step 1](step1.md)

[return to freeCAD presentation](../README.md)


