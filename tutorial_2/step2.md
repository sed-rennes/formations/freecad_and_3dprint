# Step 2: add a peg grip

## Create a sketch for draw a bump of the grip
Select up face of the pad :

![select face up of pad](images/select_face_up.png)

Then, create a sketch on this face then import external edge of the peg with `external geometry ` tool 
![Sketch workbench/external icon](https://wiki.freecadweb.org/images/b/b2/Sketcher_External.svg) and draw a closed circular segment corresponding to bumps in face image scanned :

![closed circular segment to form a bump for peg grip](images/closed_circular_segment.png)

Close de sketcher editor

## Create a bump 
Create an additive pipe ![PartDesign workbench/additive pipe icon](https://wiki.freecadweb.org/images/2/20/PartDesign_AdditivePipe.svg)  with this circular segment and a selecting edge along the deep of de peg:

![select a edge for creating a bump with additive pipe](images/additive_pipe_select_edge.png)

## Copy a bump using multi-transformation

Add a linear pattern transformation selecting the edge :
![select a edge for creating 5 copies with linear pattern](images/linear_pattern_select_edge.png)

Them add a mirrored transformation selecting `base YZ Plane` :

![select YZ plan for creating a mirrored transformation](images/mirrored_tranformation_select_YZ_plan.png)

The result will looks like this:

![image of the step2 finished](images/step2.png)

# [step 3](step3.md)

[return to step 0](tutorial.md)