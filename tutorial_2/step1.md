# Step 1: integrate a part image for help design in FreeCAD

:warning: **Next steps is different from freecad version 0.21 and 0.19** 
## With FreeCAD version 0.19 

You can integrate directly this image ![front peg scan](images/scan_front_peg.jpeg) in freeCAD using `Image` workbench ![image workbench image](https://wiki.freecadweb.org/images/d/de/Workbench_Image.svg) or transform to PNG with transparency using for example [GIMP software](https://www.gimp.org/)
for easy manipulation in FreeCAD: 

![front peg scan ](images/scan_front_peg.png)

Launch import tool ![image Workbench/image import icon](https://wiki.freecadweb.org/images/f/f1/Image-import-to-plane.svg) then select a plan `XY` to place image into with a offset of `-0.1mm` (that help to draw a sketch over)

Scanner images often has integrated information about scale that help to define good lengths (this is the case of the given images). But if you want to be sure of the scale of the image, you can use this length as reference :

![front peg scan ](images/length_front_peg.svg)

then, adjust scale with tool ![image Workbench/image scale icon](https://wiki.freecadweb.org/images/0/05/Image_Scaling.svg):
* select 2 points in `3D view` that represents ends of a segments with reference length
* type de true length of this segment
* select image in model tree 
* validate

## With FreeCAD version 0.21


Import the image: ![front peg scan ](images/scan_front_peg.png) 

directly with file menu `Import` ![Std/image import icon](https://wiki.freecad.org/images/b/bc/Std_Import.svg)

Scanner images often has integrated information about scale that help to define good lengths (this is the case of the given images). But if you want to be sure of the scale of the image, you can use this length as reference :

![front peg scan ](images/length_front_peg.svg)

Then select image and open right click menu to select `Change image` 

then, adjust scale with `Calibrate` button :
* select 2 points in `3D view` that represents ends of a segments with reference length
* type de true length of this segment
* validate

## Place the image in 3D space

Create a body placeholder using `Part Design` workbench ![Part Design Workbench/create a body icon](https://wiki.freecadweb.org/images/1/10/PartDesign_Body.svg). Then show `Origin` in model tree. You can now adjust image transformation to oriented the left part image with the axis of de body and the center of this:

![front image placed](images/front_scan_placed.png)






## Create a sketch and draw the left side of the part profile

You can now create a sketch on plan `XY` then use tools like poly-line ![Sketcher/polyline icon](https://wiki.freecadweb.org/images/b/bd/Sketcher_CreatePolyline.svg) or fillet ![Sketcher/fillet icon](https://wiki.freecadweb.org/images/6/63/Sketcher_CreateFillet.svg) to define left side profile :
![Left side of profile ](images/left_sketch_profile.png)

you can adjust lines and arc by drag and drop elements with mouse. Maybe you will need to add some constraints to points or segments to do more easy ajustement.

**Notice**: Right side of the part are not well aligned in the scan image (the part is brocken on the meddle), so we can directly draw right side but use some symmetries on design.

# Draw right side by symmetric copy on sketch

Edit de sketch and select all left side geometric elements. then select with `Ctrl + left mouse` the vertical origin line of the sketch (green line). Now launch the tool symmetry ![Sketcher/symmetry icon](https://wiki.freecadweb.org/images/3/30/Sketcher_Symmetry.svg)
This tool copy all geometries as mirror of the left side. 
You can see that right side of the part in the image is not well aligned with the fresh copied geometry. 

**You need to close the profile by connected with coincidence contraint the axial points**. You also need to modify right teeth that actually are not symmetric to left teeth:

![zoom on teeth profile](images/teeth_sketch_profile.png)

# Create a Pad
Create a pad with this sketch with almost 13 mm deep and set this pad to be symmetric to sketch plan. 

![image of the step1 finished](images/step1.png)

# [step 2](step2.md)

[return to step 0](tutorial.md)
