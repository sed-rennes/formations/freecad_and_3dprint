# Step 3: Shaping lateral profile of the part

You can see in the side scanned image of the peg that its profile is a sort of oblong shape:
![Scanned image of side of peg  ](images/scan_side_peg.jpeg)

To shape this we need to cut some material from or precedent part design.

## add this image of the side scanned part in freecad and scale it (48 mm of part length) :

![Front peg scan ](images/scan_side_peg.png)

## Place in the 3D space of FreeCAD the image 

Just touching a side of part like this (preserving symmetry axis): 
![Placing side scan touching side face of part](images/placing_side_scan.png)

## Select side face of `Pad` feature and create a sketch

![Selection of face from Pad feature](images/select_face_side.png)

## draw the oblong form in sketch using image as reference

keep in mind we can cut the current part all volume outside the oblong form. So we need to add a external limit to form future pipe to be subtracted to current part. This is why we need to add a external rectangle :

![Sketch draw of side oblong form with external rectangle](images/side_sketch.png)

## draw a single segment parallel to X axis in a new sketch
to define a subtractive pipe based on profile os precedent sketch, we need to define a segment to follow in sweep generation. You can do that by draw a single horizontal (parallel to X axis) segment on a new sketch :

![Define a segment in another sketch for horizontal sweep](images/define_segment_for_sweep.png)

## Create a `subtractive pipe` ![PartDesign/subtractive pipe icon](https://wiki.freecadweb.org/images/0/0b/PartDesign_SubtractivePipe.svg)

Selecting sketch of oblong profile and add edge corresponding to segment in the other sketch :
![parameters and subtractive pipe image](images/parameters_subtractive_pipe.png)

The result will looks like this:

![image of the step3 finished](images/step3.png)

# [step 4](step4.md)

[return to step 0](tutorial.md)