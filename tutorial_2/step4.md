
# Step 4: Make a chamfer along external faces of peg

I will be interesting for confort to make a chaffer along external faces of peg that will be manipulated by users:

![image of chamfer to construct](images/chamfer.png)

Standard tool for chamfer ![PartDesign/Chamfer icon](https://wiki.freecadweb.org/images/e/ee/PartDesign_Chamfer.svg) fails to build the right chamfer because the bumps: Its is hard to define a good chamfer with edges intercepting bumps. 

This is why, next you will create a section definition of chamfer and make a subtractive pipe using edges of a sketch defined in [step 3](step3.md) as path of the pipe:

![selection of edges to define subtractive pipe path](images/select_sketch_edges_for_pipe.png)

## Create a Datum plane to support a sketch defining section of chamfer 

Select all the edges of sketch defining the future path of subtractive pipe (you can hide others features for easy selection)

Them add a `DatumLine` with Part Design tool ![Part Design/datum line icon](https://wiki.freecadweb.org/images/8/8a/PartDesign_Line.svg)

This tool automatically create a line perpendicular to selected path using first principal axis :

![image of datum line constructed](images/datum_line_parameters.png)

Now, create a `DatumPlane` based on this `DatumLine` them select `Object's YZ` as attachement mode :

![image of datum plane constructed](images/parameters_datum_plane.png)

## Create a sketch with the subtractive pipe profile
Now this DatumPlane will be used as support for make a sketch to draw the section of subtractive pipe :

![image of profile of subtractive pipe](images/profil_subtractive_pipe.png)

## Create subtractive pipe
using precedent sketch as pipe profile and path selected in sketch of step3, you can define the subtractive pipe :
![image of subtractive pipe definition](images/subtractive_pipe_definition.png)

## Finally create a mirrored feature ![PartDesign/Mirrored icon](https://wiki.freecadweb.org/images/2/21/PartDesign_Mirrored.svg)

Define mirrored feature using `Base YZ plane`:
![image of mirrored transform definition](images/symetric_transform.png)

The final result will looks like this:

![image of the step4 finished](images/step4.png)

# [step 5](step5.md)

[return to step 0](tutorial.md)