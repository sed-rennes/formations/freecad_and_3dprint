 # Basic introduction of FreeCAD software for part modeling to send to 3D printer

## Overview 
This is a short introduction to FreeCAD software that help to design parts suitable for 3D print process. After this course and practices you will can:

* know how to define solids by surfacic modeling,
* define parts by parameters and constraints,
* define complex forms by combining different tools such extrusion, revolution, additive or subtractive operation,
* know basics aspects of 3D print process using fused filament fabrication (FFF)

## Introduction
Today in our Lab, we have facilities for 3D print parts using fuse filament fabrication method ([FFF](https://en.wikipedia.org/wiki/Fused_filament_fabrication)) with plastic materials as PLA and ABS. We applying FFF for rapid prototyping cycles in many research domains such humain-machine interface, robotics, internet of things but also in maintenance for replacing or tunning some parts. 

But FFF is just one of many technics in the field of Computer-aided manufacturing ([CAM](https://en.wikipedia.org/wiki/Computer-aided_manufacturing)).
There are many addictive technics like FFF (also know as FDM for fused deposition modeling), stereo lithography, [etc.](https://en.wikipedia.org/wiki/3D_printing_processes#Processes). There are also [subtractive technics](https://en.wikipedia.org/wiki/Numerical_control#Other_CNC_tools), but for all of this, make a part needs almost always to model it before manufacturing.

The process of modeling a part using commonly computer-aided design (CAD) softwares. Let's try to see next why 

## Part modeling

To define a part for CAM production, need at least to define the _form_ of the part: this form splits the world in only two domains: 
* _inside_ of the part and
* _outside_ of the part. 

The form of the part can be see as mathematical solid.

### Solid modeling

You can define the form by a mathematic définition lik implicit 3D coordinates equations of inequalities such :

```math
x^2 + y^2 + z^2 < 4
```
to define a sphere of radius = 2, if a point P of coordinates (x,y,z) respect this inequality them P is inside the part else P is outside de part. 

you can add others equations or inequalities to define more complex forms (unions, interceptions, differences) with constructive solid geometry ([CSG](https://en.wikipedia.org/wiki/Constructive_solid_geometry)) 

### Surface modeling
Another way to define the form of a part is to define the frontier of this part : a **oriented surface** that say what side of the surface is inside and what side if outside :

![surface oriented frontier](inside_outside.svg)

At every point on the frontier surface it is defined a _direction_ vector that point to outside (by convention). Mostly this direction vector is a normal vector to the surface.

But to complet definition of the form of the part with a oriented surface, this surface must respect some topological constraints :
* be continues with no hole in surface (but the part may have holes like a tore)
* not self-intercepting
* not nul nor degenerate ( example : a cylinder must have a radius greater that zero)


Some examples:

### This is not a solid (revolution surface not closed)
![a bol surface](surface.png)
This is open surface with not inner volume

### This is a solid (surface with a thickness = solid)
![a bol surface](thin_solid.png)
Add a thickness to the open surface to produce a solid shape

#### This is not a solid (two solids intercepting)
![a cube and a cylinder intercepting](not_fusion.png)

Either you have 2 different solids, either there are self-intercepting surfaces so the object is not a solid 

#### This is a solid (two solids boolean fusion)
![a cube and a cylinder in boolean fusion](fusion.png)

Doing true boolean fusion, the inner self intercepting surfaces are removed.

#### **This is not a solid (Klein bottle)** 

![A two-dimensional representation of the Klein bottle immersed in three-dimensional](klein_bottle.png)

you can not define outside and inside of the bootle.

### **This is a solid (double tore)** 
![double tore](doubleTore.png)

This oriented surface is continus but define well a solid with 2 holes 

### **This is not a solid (incoherent orientation in surface assembly)**
![incoherent orientation in surface assembly image](not_well_oriented.png)
Some part of the surface of the object has oriented in a incoherent way in regard to others. Reverse orientation of all part of the surface not solve these incoherency. 

#### Surface definition with NURBS Shapes

A way to define oriented surfaces that catch some current forms like plans, sphere sectors, cylinder, but also loft is to use NURBS (for Non-Uniform Rational Basis Spline).
NURBS has a efficient mathematical formulation of a courbe or surface representation but also has a easy human interaction. 

It is possible to set mathematic continuity of NURBS and slope and courbure continuity.

![an example of NURBS curve edition in FreeCAD](nurbs_curve.png)

this is a edition interface of a nurbs curve in FreeCAD, user can change de control points (red dots) interactively and adjust the weight of this (changing radius of  blue cercles)


![an example of NURBS generated by FreeCAD](nurbs.png)

In this example, a smooth surface across three different sections is defined using patches of NURBS. One of this patches is showed in green.
NURBS is a good trade-off between precision of a solid surface approximation while keeping surface definition light and simple and accessible for human design.

#### Mesh shapes (soup of triangles)
Another simple representation of a surface was widely used on 3D Print software: The mesh. 

A mesh is a set of triangles (sometimes quadrilaterals)
tha define a polyhedral surface. 

![example of mesh representation of a object surface](mesh.png)

To get oriented surfaces, every triangle must be defined by 3 points with an order. This order will be used to define oriented normal of a triangle. 

for example, let set a triangle between points A,O,B. we can define a normal **N** of this triangle using

```math
 	\vec{N} = \overrightarrow{OA} \times \overrightarrow{OB}
``` 

the resulting vector **N** indicate for convention the outside of the mesh.

![a oriented triangle definition](oriented_triangle.svg)

(**N** is placed on center of the triangle here for simplification)

Mesh are often the only supported type of surface representation available for softwares used to generate commands for 3D print (slicers).

Softwares like FreeCAD have fonctions for export a shape in mesh formats like STL or OBJ.

## Classic workflow

![a classic workflow to produce 3D parts](workflow.svg)

It is interesting to see what is the classic workflow to produce 3D printed parts. 

### From ideas to 3D print
You can start in your head with a pure new concept, then try by iterations to fine design using CAD tools as FreeCAD, libreCAD or OpenSCAD. After modeling you may produce a mesh file ready for 3D printer software or sometime you need to repair or adjust mesh before.

### From real world to 3D print
Sometimes you have a part broken or you need to adapt it to use in unplanned way/situation. So you need to start from plans "blue prints" or some specification. If theses informations are missing, you can try to do a kind of renverse engineering to catch theses. For example you can uses mesure tools or scanner to produce digital reproduction of parts. Direct output of 3D scanner if often not suitable for print, you may pass either by a repair mesh tool or a redesign using modeling tools.

At the end usage of a slicer tool is necessary to transform mesh files in a sequences of numerical commands for the printer device.


## FreeCAD

[**FreeCAD**](https://www.freecadweb.org/) is a open source software for Computer-Aided Design (CAD) with many [features](https://wiki.freecadweb.org/Feature_list#General_features:):
* Multi-platform
* Powerful geometry kernel based on OpenCASCADE Technology for :
    * parametric modeling
    * complex boolean operations, fillets, extrusions, etc.
    * NURBS and B-REP surface representation
    * import/export in standard formats as STEP and IGES
* 2D sketcher editor with constraint solver to produce 2D shapes used in 3D solids generations with transformations (extrusion, sections or fillets) 
* python large integration for :
    * customise GUI elements
    * automatic processing scripts
    * import FreeCAD as python module for geometry computing
* plugin extensions
* Finite Element Method (FEM) tools
* ...

![Example of FEM in FreeCAD](FEM.png)
Example of finite element method applied to compute deformation and constraints into a cantilever.

![Example of robot trajectory tool in FreeCAD](robot.png)
Example of robot end tool trajectory setup 
## Others opensource tools

There are many closed source CAD software, like SolidWorks, Catia, Autocad, etc. Here we are focusing on open source softwares. This is not a exhaustive list of open source software by there is a list of some useful tools for a workflow to design to print.

### OpenSCAD
OpenSCAD is a powerful tool for procedural script CAD building with CSG and extrusion operations. It is based [CGAL library](https://www.cgal.org).
OpenSCAD is well suited for mesh CSG operations with better memory footprint that FreeCAD for mesh manipulation

### AliceVision Meshroom

[AliceVision Meshroom](https://alicevision.org/#meshroom) is a suite of tools designed for easy mesh reconstruction based of photogrammetry technics: take a lot of photographies around a object and produce an approximation of the mesh of the object. This is a digitaliser tool.

### MeshLab

IS a tool for some complex mesh manipulations. For example is a useful tool for remove isolate internal triangles or holes in a complex mesh computed from point clouds scanners.

### Inkscape
Inkscape vectorial 2D draw tool is a tool. It is a very useful tool for simple measurement based on PDF output from ours photocopiers.     

### LibreCAD

Is CAD software for 2D sketching. It allow DXF file format manipulation. It is suitable for plans editions and will be useful in some context to share plans with industrial partners.


# 1st Tutorial : from designer idea to 3D print part
In this tutorial we try to modeling a part from a technical plan of brick. This is a concept of a new brick based on compressed soil from a local designer. The purpose of this tutorial is:
* discover basic modeling tools in FreeCAD like sketch editor and Part Design Workbench
* understand the concept of constraints in sketchs
* manipulate boolean additive and subtractive operations
* attach sketchs to part faces and define constraints from external elements of this
* using patterns operations for duplicate features

let's go : [tutorial #1](tutorial_1/tutorial.md)

# 2nd Tutorial : Print a broken part replacement

Imagine you have a broken part, you do not how to buy a replacement part or replacement part is hard to give for reasons. Then maybe you can try to print it !

let's go : [tutorial #2](tutorial_2/tutorial.md)

TODO


