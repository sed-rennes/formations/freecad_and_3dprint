# Step 12 : Manual Chamfer to brick eye

![step 12](images/step12.png)

In this step you can use subtractive cone to create a chamfer around a brick eye. To easy find the position of the cone, you can define a DatumLine as axis of brick eyes

1. select an arc of cercle in one eys like this : ![selection of an arc](images/select_arc_for_axis.png)
2. click on button ![PartDesign/DatumLine icon](https://wiki.freecadweb.org/images/8/8a/PartDesign_Line.svg) and validate. There is now a axis aligned with de axis of the eye.
3. Start adding a subtractive primitive ![PartDesign/subtractive primitive icon](https://wiki.freecadweb.org/images/thumb/4/48/PartDesign_CompPrimitiveSubtractive.png/72px-PartDesign_CompPrimitiveSubtractive.png), then select cone ![PartDesign/subtractive cone icon](https://wiki.freecadweb.org/images/6/6d/PartDesign_Subtractive_Cone.svg)
4. In `Attachment` parameters select the `DatumLine`
5. Set parameters of the `Cone001` to respect a 45° angle of slope:
    * `Radius1`: `Spreadsheet.eye_diameter / 2 + Cone001.Height`
    * `Radius2`: `Spreadsheet.eye_diameter / 2`
    * `Height`: 10 mm
    * Z offset: -5 mm


# Final step

Applying again Multi-Transformation to eye chamfer but using mirror pattern then linear pattern. For Mirror pattern, you need yo create a datum plan ![datum plan icon](https://wiki.freecadweb.org/images/e/e5/PartDesign_Plane.svg) in the meddle of the brick parallel to `XY_Plan` :
![mirror plan for the brick](images/datum_plan.png)

[return to step 10 & 11](step10.md)