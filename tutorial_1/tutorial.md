# step 0 : discover FreeCAD GUI
## install FreeCAD version 0.18 or newer.
### Linux
binaries of freeCAD v 0.18.x are available in standard package in some distributions (Ubuntu, Debian, OpenSUSE, Gentoo, Fedora, Arch)

It is possible to install a self contained AppImage here : https://www.freecadweb.org/downloads.php

### Mac OS

see: https://www.freecadweb.org/downloads.php for binary DMG package

### Windows

see: https://www.freecadweb.org/downloads.php for binary windows 32 bit or 64 bits installers


### From sources 
See here : https://github.com/FreeCAD/FreeCAD#compiling

### Reference version 
Next suppose you have a fresh version 0.18.xx of FreeCAD. 

( 
    Tested with
```
v0.18.4OS: Ubuntu 20.04.1 LTS
Word size of OS: 64-bit
Word size of FreeCAD: 64-bit
Version: 0.18.4.
Build type: Release
Python version: 3.8.2
Qt version: 5.12.8
Coin version: 4.0.0
OCC version: 7.3.0
Locale: French/France (fr_FR)
 ```
 )

## Launch FreeCAD

at console you can launch FreeCAD with some options:
```bash
   freecad --help
```
show this message :

```
FreeCAD

For detailed description see http://www.freecadweb.org

Usage: FreeCAD [options] File1 File2 ...

Allowed options:

Generic options:
  -v [ --version ]          Prints version string
  -h [ --help ]             Prints help message
  -c [ --console ]          Starts in console mode
  --response-file arg       Can be specified with '@name', too
  --dump-config             Dumps configuration
  --get-config arg          Prints the value of the requested configuration key

Configuration:
  -l [ --write-log ]        Writes a log file to:
                            /home/andradgu/.FreeCAD/FreeCAD.log
  --log-file arg            Unlike --write-log this allows logging to an 
                            arbitrary file
  -u [ --user-cfg ] arg     User config file to load/save user settings
  -s [ --system-cfg ] arg   System config file to load/save system settings
  -t [ --run-test ] arg     Test case - or 0 for all
  -M [ --module-path ] arg  Additional module paths
  -P [ --python-path ] arg  Additional python paths
  --single-instance         Allow to run a single instance of the application

```

You can see that it is possible to launch en console mode with option `--console`. This mode is useful for automatic computation models using python langage (for example to produce series of parts using different parameters)

For start, you can launch just with default options. this open a GUI windows:

![start FreeCAD GUI in french](images/start_FR.png)
(in this test GUI language is set to French)
## Set the interface language
to set the langage to the FreeCAD GUI  go to `Edit`/`Édition` menu, them `Preferences`/`Préférences` sub-menu. 
After `Preferences`/`Préferences` windows opening click on `General`/ `Général` tab. You can choice the language on `Changer la langue`/`Change language:
![Set language option](images/set_language_fr.png)  

You can restart FreeCAD to completed setting:

![start FreeCAD GUI in English](images/start_EN.png)

Next, you can assume all interfaces are set to english. (Feel free to use FreeCAD in your preferred language and propose you translation to this tutorial)

## Create a new document
Create a new document, key `Crtl+n` or  menu `File => New` or click on button:  
![new button icon](https://wiki.freecadweb.org/images/d/d4/Std_New.svg)

You can see something like this: 
![new document windows](images/new_doc.png)

### Elements on the GUI
![elements of GUI](images/gui_elements.svg)

Here you can distinguish several components. Main are:
* **3D View** : show parts in a OpenGL screen 3D projection with different rendering options
* **Combo View** : this frame is divided in two parts:
    * Upper: with *tree view* that show structure of objects in document and *task panel* that show command currently waiting for parameters 
    * Lower: that show properties editor of objects
* **The Workbench selector** allow you to switch between several workbenches. Every workbench has a specific set of tools.

There are others interesting components on the GUI, see here: https://wiki.freecadweb.org/Interface

#### Set navigation style in 3D View
you can change the style of navigation style in 3D View :
* Click right into 3D View the select `Navigation styles` menu 
* Choice the style `Gesture`

![Changing navigation style](videos/set_style_gesture.webm)


This navigation style allow rotation, slide, zoom, object selection with only mouse button (3 button mouse recommended )  

If you pass the mouse over `Gesture` in the down left part of the screen, you can get a popup with some explanation about this mode :

![Gesture popup](images/gesture_popup.png)

![Example of gesture navigation style usage](videos/gesture_navigation.webm)

**NOTICE** : We assume this choice for the whole tutorial until we explicit set another choice.

Next you can explore test all this GUI elements build a simple part. 

## Create a simple part

We were inspired by from this concept :
https://www.thingiverse.com/thing:2786202 to design a very simple support for pen using simple CSG (Constructive Solid Geometry).

1. switch to `Part` workbench ![part icon](https://wiki.freecadweb.org/images/0/04/Workbench_Part.svg) ![switch to Part Workbench](videos/switch_to_part_WB.webm). now you can see many new buttons
2. Create a new cone solid, click to button ![Part/cone icon](https://wiki.freecadweb.org/images/3/35/Part_Cone.svg) or menu `Part -> Primitives -> Cone`
3. Change the view to isometric view, with key '0' (zero key) or click in button ![isometric icon](https://wiki.freecadweb.org/images/b/b1/Std_ViewIsometric.svg) or menu `View -> Standard View -> Axonometric -> Isometric` (or drag an drop with right mouse button to set a convenable view) 
4. select the Cone with a left click on cone object or selecting in combo view the `Cone` object in `Model` tab
5. Edit Cone properties by properties editor in lower part of combo view inside `Data` tab. Set `Radius1` to 17.5 mm, `Radius2` to 9 mm and `Height` to 30 mm ![Create a cone and set dimensions](videos/create_cone_and_set_dim.gif)
6. Create a sphere at the bottom to make the pen oscillate : click to button ![Part/sphere icon](https://wiki.freecadweb.org/images/d/de/Part_Sphere.svg) or menu `Part -> Primitives -> Sphere`
7. adjust Sphere radius to 10 mm
8. Change the position of the Sphere center to (0,0,7mm) several possibilities to choice :
    * **Edit Sphere properties** in lower part of combo view :`Data -> Base : Placement -> Position -> z` set to 7 mm
    * Open `Placement` task with menu `Edit -> Placement ...` then apply a translation of 7 mm in Z axis.
    * Open `Transform` task (In upper part of combo view in `Model` tab) by double click on `Sphere` in objects tree. Set translation increment to 1 mm then drag and drop de Blue arrow based on center of Sphere in 3D view to set the good translation (not so easy to set 7 mm) ![Set Tranformation with mouse](videos/set_transformation.gif)
9. Select both `Sphere` and `Cone`: with `control+left click` on every object either in 3D view or in Object tree in `Model` tab in combo view 
10. Build a **boolean union** between `Sphere` and `Cone` , click to button ![Part/union icon](https://wiki.freecadweb.org/images/a/a7/Part_Booleans.svg) or menu `Part -> Boolean -> Union`. Now there is `Fusion` object on the _root_ and in the _leaf_ level there are `Sphere` and `Cone`.
12. Selected Fusion object and set its transparency to 50%, type `Ctrl+d` to open _Display properties_ dialog box or in lower part of combo view go to `View` tab the edit `Transparency` property to 50%. If you unselect Fusion object, you can see by transparency the result of boolean operation 
11. Create a new cone then change `Radius1` to 2 mm, `Radius2` to 6.5 mm and `Height` to 30 to construct de placeholder of the pen.
12. Select in this order `Fusion` then `Cone001`
13. Build a **boolean cut** between `Fusion` and `Cone001` , click to button ![Part/cut icon](https://wiki.freecadweb.org/images/f/fb/Part_Cut.svg) or menu `Part -> Boolean -> Cut`. 
14. Select the sharped edges of `Cut` object by 'Ctrl + left click' on theses:
![selection of sharped edges](images/select_sharped_edges.png)
15. apply **Fillet** tool, click to button ![Part/fillet icon](https://wiki.freecadweb.org/images/3/30/Part_Fillet.svg) or menu `Part -> Fillet`. Choice a radius of 1 mm (not more) to round the edges. Your part is almost ready to print:
![final pen support](images/final_pen_support.png)
16. select 'Fillet' object and Export as STL format, in Menu  `File -> Export` the select `STL mesh` the save as `my_pen_support.stl`

After print this look like this:

![Real printed support photo](images/pen_support_real.jpg)

## Deal with implicites and explicites dependencies in a part

You can see that forms used to produce this part has some dependencies, for example the height of the cutting cone must correspond to the height of the first plain cone. Another implicit constraint is that the sphere and all cones needs to be aligned in Z-axis. For a simple part like this, it is not difficult to find good positions and geometries parameters to respect implicit constraints for a defined dimension of the part. But if you are design a prototype maybe you need to pass iterative conception/try cycles and you will need to adjust dimensions of the part and then need to recompute some positions and sizes of basic forms. This task will be tedious and demand lot of changes for complex parts. FreeCAD helps you with this by the usage of mathematic expressions for define parameters and for some implicit contraints, FreeCAD offerts set of tools that help you build dependencies between geometries and automatic solve this when changes become. One of theses set of tools is the **PartDesign Workbench**

After this start part using simple primitives and booleans operations, next you can learn to use the **PartDesign Workbench** to create a special brick :

# The Brick
 
Next 12 steps are related to a Part designed by Mr. [Alain Schwartz](https://www.linkedin.com/in/alain-schwartz/). 

Mr. Schwartz explains (in French) the context of this part here : 
[French video of Hello-Terra company](https://gust.com/companies/helio-terra/)

The concept is a particular form of a compressed mud-brick and a process to assembly. 

![concept of brick BTC Helio-Terra](images/HELIO_TERRA_BTC.svg)

**Important** : Mr. Schwartz gently accepts the utilisation of this part in this tutorial. Please don't diffuse these material outside INRIA without explicit autorisation of him.

The next step allow you to create de basic volume of the brick :

# [step 1](step1.md)

[return to freeCAD presentation](../README.md)

