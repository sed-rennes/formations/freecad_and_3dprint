# Step 4 : Add a longitudinal bump to the brick

![step 4](images/step4.png)

Here you need to do basically the same task done for [step 2](step2.md) but we need to overpass the bumps already in place.

## Display or hide objects

Lets go to `Model` tab in combo view. You can see something like that:
![Model tab in combo view](images/combo_view_model.png)

You can see the Body active `Body`. You also can see `Pad` and `AdditivePipe` but theses objects or features are _grised_. This _grised_ state indicate that feature or object are invisible or hide in 3D View. 
You can switch to visible using `Espace` key. In case of features of a body, only one can be visible at time. You can choice to display a precedent step in your part history. Then it is possible to work with a reference to this _past_ as reference for construct a new step.

1. Make visible `Pad`
2. Select a face from `Pad` like this :![Select face YZ](images/select_face_yz.png)
3. create a new sketch based on this face
4. create a section for longitudinal bump like in [step 2](step2.md) (dont forget to refer to `Spreadsheet` values)
5. start creation of a new additive pipe
6. In combo view switch to `Model` tab view the show `Pad` and hide `LinearPattern`.
7. Switch to `Tasks` tab view in combo view the as for step 2, set `Corner Transition` to `Right Corner`, add edges as showed here (in green):
![parameter of additive longitudinal pipe](images/additive_longitudinal_pipe_parameters.png)
8. validate and possibly refresh by a double _hide/view_ on `AdditivePipe001` (possibly bug in version 0.18.4)

## Dependency graph
An useful tool for debug some situations on FreeCAD is the dependency graph (menu `Tools => Dependency graph...`) :
![Dependency graph for step 4](images/dependency_graph.svg)

You can see : 
* how objects depends on Spreadsheet values
* dependencies from `Pad` object
* dependencies from `X_Axis` and `XY_Plane` from origin
* etc.

# [step 5](step5.md)

[return to step 3](step3.md)