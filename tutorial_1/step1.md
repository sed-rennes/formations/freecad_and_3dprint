# Step 1: basic usage of sketch editor to produce the volume of the brick

Here you can define the basic volume of the brick with parameters that can change using mathematic expressions and contraints in sketch editor.

![result of step1](images/step1.png)

## The PartDesign Workbench

One of the main concepts of FreeCAD is the PartDesign Workbench that allow you to produce complex parts by iterative modifications of a simple part using additive or subtractive operation that reference face, edges or inertial plans of the precedent iteration part as implicit parameters. Theses references or dependencies allow you to change or adjust fondamental dimensions without the need to rearrange all dependant operations. 

1. Open an new document and save has `brick.FCStd`
2. switch to PartDesign Workbench ![PartDesign Icon](https://wiki.freecadweb.org/images/3/39/Workbench_PartDesign.svg).
3. Create a new body, with button ![PartDesign/Create Body Icon](https://wiki.freecadweb.org/images/1/10/PartDesign_Body.svg). A _body_ is a holder for frames, origins, axis and operations related to a part. By default, the new body is activated after creation. If you have many bodies, only one can be activated at time. You can activate a body by double click on body object in `Model` tab (upper part of combo view). if you browse contents of the body you can see that it already host a object `Origin` (not visible) and 3 axis (`X_Axis`, `Y_Axis`, `Z_Axis`) and 3 planes (`XY_Plane`, `XZ_Plane`, `YZ_Plane`) ![body compounds](images/new_body.png). You can display all by selecting `Origin` in model tab, then press `ESPACE` key. 
4. Select the `XY_Plane`
5. Create a new sketch in `Body` with ![PartDesign/Create Sketch icon](https://wiki.freecadweb.org/images/f/f1/Sketcher_NewSketch.svg). A Sketch allow you to create 2D plan with geometric elements and associate constraints. At creation, the selected plan `XY_Plane` is set as support of `Sketch` object. It is also possible to select a face from already defined geometry in body as support for a new sketch. The creation of the new sketch automatically open sketch editor and switch to the Sketcher Workbench
6. You can create a rectangle with ![Sketcher/Rectangle icon](https://wiki.freecadweb.org/images/a/a2/Sketcher_CreateRectangle.svg) or press `r` key. With mouse `left click` in 3D view to set 2 extremes points of the rectangle. This tool creates actually many geometry elements (4 lines: every line attached to 2 points) and add contraints to theses geometries ( each 2 lines are _coincident_ end points, 2 lines are _horizontal_ and the others are _vertical_) you can see all theses in `Tasks` view in combo view when sketch editor is open : ![elements and constraints view after adding ](images/elements_and_constraints_rectangle.png). The constraint solver compute a minimal position to every point in the sketch that respect with some tolerance all constraints in sketch. You can see `Solver messages` in upper part of `Tasks` tab in combo view ![solver messages capture](images/solver_messages.png). This windows show 4 degrees of freedom (DoF) in the sketch (because you you can move 2 points independently (2 coordinates each). You can see this _freedom_ by drag an drop points or lines in 3D view. 
7. Add a distance constraint ![Sketcher/Distance icon](https://wiki.freecadweb.org/images/c/cf/Constraint_Length.svg) (`Maj+d` key) for a vertical line and a horizontal line (`left click` on line to select). You can set the values of theses distances respectively to 200 mm and 400 mm (according to Mr. Schwartz schema). You can see that the number of DoF is now equal to 2.
8. select origin of sketch and one corner of rectangle, then add a coincidence constraint ![Sketcher/Coincidence icon](https://wiki.freecadweb.org/images/e/ea/Constraint_PointOnPoint.svg) (`c` key). Now the sketch is fully constraint with a DoF = 0. lines are now showed in green ![Fully constraint rectangle](images/fully_constraint_rectangle.png)
9. close de sketch editor, `escape` key or close button in `Tasks` tab in combo view.
10. Selects `Sketch` object
11. Create a pad, click on ![PartDesign/Pad icon](https://wiki.freecadweb.org/images/f/f8/PartDesign_Pad.svg) or menu `Part Design => Pad`. Set Length to 100 mm in `Pad parameters` dialog in `Tasks` tab then validate.

## handle parameters and expressions

The volume of the brick is well defined, but if you want now to change the length or the height of the brick you need to find were theses parameters are defined. In the case of this brick, you need to change `Pad` height parameter, open `Sketch`, then change the value of a distance constraint. As a best practice will be, as in code programming to define _variables_ and change just the variables to recompute all elements. FreeCAD can define in many inputs for parameters as expressions using the name of others internal parameters. Lets see an example. 
The brick has theses dimensions :

| Parameter 	|  Value 	|
|:---------:	|:------:	|
| Height    	| 100 mm 	|
| Width     	| 200 mm 	|
| Length    	| 400 mm 	|

One can remark that all this parameters are some ratio to a _base size_ of 100 mm :

| Parameter 	|  Value 	|    Expression   	|
|:---------:	|:------:	|:---------------:	|
| Height    	| 100 mm 	| 1 x base_size 	|
| Width     	| 200 mm 	| 2 x base_size 	|
| Length    	| 400 mm 	| 4 x base_size 	|

**Notice** : this will be a pure coincidence, but in the case of Mr. Schwartz design it seems not: he plans to have half-bricks. So suppose that this ratio make sense.

Now you can use Spreadsheet in FreeCAD to define in one place main values that you can refer to define other secondary values like Height, Width etc.
### Spreadsheet 
FreeCAD can manipulate simple spreadsheet with values and expressions. Lets try !
1. Switch to Spreadsheet workbench ![spreadsheet icon](https://wiki.freecadweb.org/images/b/be/Workbench_Spreadsheet.svg)
2. Create a new spreadsheet with button Switch to Spreadsheet workbench ![create spreadsheet icon](https://wiki.freecadweb.org/images/f/f9/Spreadsheet_CreateSheet.svg) or menu `Spreadsheet -> Create spreadsheet`
3. Display the `Spreadsheet` by double click in `Spreadsheet` object in `Model` tab. That open a new windows in FreeCAD. You can edit to put in a cell values and text like this: 
![some spreadsheet cells](images/spreadsheet_cells.png)
4. Left click on `Sketch` object in `Model` tab in upper part of combo view. Then you can navigate in lower part of combo view in `Data` tab to find `Constraints` then you can see distance constraints with names like `Constraint9` : ![list of distance constraints](images/list_distance_constraints.png)
5. Edit expression of the constraint `Constraint9` (with value =400 mm) on expression editor click on ![expression icon](https://wiki.freecadweb.org/images/b/b2/Sketcher_Expressions.png) in right of value. Put an expression like `Spreadsheet.B2 * 4`, where `B2` correspond of the cell coordinate in the spreadsheet like :
![set expression using spreadsheet cell](images/set_expression_using_spreadsheet_cell.png)
6. Set expression for the other constraint (with value = 200 mm) with something like `Spreadsheet.B2 * 2`
7. Set expression for pad Height to something like `Spreadsheet.B2` by double click on `Pad` Object or after selecting pad editing property `Length` in `Data` tab.
8. Test changing values of base size in `Spreadsheet` to see if the form of the brick is still conserved.

See more details on expressions here : https://wiki.freecadweb.org/Expressions

## Naming cells and constraints

Using expression with coordinates of cells will be a little tediou. FreeCAD allow you to add a alias to cells. To do that, `right click` on a cell in a spreadsheet, then click on contextual menu `Properties`: a dialog box appears, go to `Alias` tab the put a name in `Alias for this cell` field ![set alias in a cell](images/set_alias.png). Now you can use this name in expressions like `Spreadsheet.base_size * 2`.

Constraints in sketch editor can be named in similar way: double click in `Contraint` tab or after selecting the constraint, type `F2` key to rename: ![naming constraint](images/naming_constraint.png). Now you can use this name in expressions like : `Sketch.Constraints.brick_length / 2.0`

# [step 2](step2.md)

[return to step 0](tutorial.md)

