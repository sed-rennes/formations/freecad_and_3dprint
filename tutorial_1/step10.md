# Step 10 : Apply a chamfer to brick

![step 10](images/step10.png)

There is a chamfer tool in Part Design workbench but in this case application conduct to bad computation for FreeCAD in bumps and channels. To overpass this limitation, we can apply a manual chamfer applying Pocket tool to a sketch with a particular session on `Pad`:
![Section for chamfer with a pocket](images/section_for_chamfer.png)

This is a zoom of constraints and geometries for on corner:

![Corner in sketch for chamfer with a pocket](images/constraints_in_chamfer_corner.png)

**Notice**: this operations are a little tedious, you can accelerate the process using key shortcuts to apply constraints and construct some geometric elements.

# step 11

You can apply this method to all directions of the brick

# [step 12](step12.md)

[return to step 9](step9.md)