# Step 5 : Create a channel on the brick

![step 5](images/step5.png)

First reflex you might have to do the channel in the brick is use a subtractive pipe tool ![PartDesign/Subtractive pipe icon](https://wiki.freecadweb.org/images/f/f4/PartDesign_Subtractive_Pipe.svg). But that will be a little bit more complicated !
Because you need to create a channel by subtracting material to a brick with already add bump. Lets zoom to this :
![Distance for dig a channel](images/zoom_channel.png)
If you _dig_ a channel you need now translate up to 105 mm because the bump instead of the distance of height of the `Pad` (100 mm).

So you can apply another tool in this step : the Pocket tool ![PartDesign/Pocket](https://wiki.freecadweb.org/images/9/93/PartDesign_Pocket.svg). This tool as a automatic _Through all_ mode make the pocket.

1. Create as step 2 or step 5 a sketch on this face : ![select down face of the brick](images/select_face_down_xy.png)
2. Put in this sketch a section of the channel and validate ![channel section](images/channel_section.png)
3. Select the sketch `Sketch003`
4. Launch the tool pocket
5. In `Pocket Parameters`:
    * Set `Type` to `Through all`
    * Check `Symmetric to plane (to _dig_ in both perpendicular directions to section)

# [step 6](step6.md)

[return to step 4](step4.md)