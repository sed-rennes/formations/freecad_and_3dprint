# Step 3 : Duplicate de bump

![step 3](images/step3.png)

Yu can duplicate a additive or subtractive element (feature) following a direction with linear pattern tool 

1. select `AdditivePipe`
2. click on button ![PartDesign/Linear Pattern icon](https://wiki.freecadweb.org/images/f/fa/PartDesign_LinearPattern.svg)
![view of Linear Pattern parameters](images/linear_pattern_parameters.png)
3. Set `Direction` to `Base X axis` in LinearPattern Parameters windows inside combo view
4. Set `Length` to expression `Spreadsheet.base_size * 2`
5. Set `Occurrences` to 2
6. validate
# [step 4](step4.md)

[return to step 2](step2.md)