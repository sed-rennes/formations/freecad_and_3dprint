# Step 2 : Create a bump along the brick

![step 2](images/step2.png)

To create a bump along de brick or a complementary channel, we need to define some parameters:
|       Parameter       	| Value 	|
|:---------------------:	|:-----:	|
| bump/channel diameter 	| 30 mm 	|
| bump/channel offset       | 10 mm 	|

![Bumps and channels](images/channels_and_bumps.svg)

Add this parameters in `Spreadsheet`:
![add more parameters in spreadsheet](images/spreadsheet_more_parameters.png)

One way to create a bump is to draw a circular sector then use it as section of a pipe. This pipe must follow the edges of the brick.

## Create de bump section

1. Select with `left click` in 3D view face of `Pad` : ![selecting a face](images/select_face_zx.png)
2. Create a sketch based on this face
3. Create a arc with 2 points and a center with button ![Sketch workbench/arc icon](https://wiki.freecadweb.org/images/0/08/Sketcher_Arc.svg)
4. close this arc with a line using button ![Sketch workbench/arc icon](https://wiki.freecadweb.org/images/3/32/Sketcher_Line.svg). you can automatically create coincidence between ends of arc and line at creation of the line or after it with button of coincidence constraint :
![video create an arc and close with a line](videos/create_section_arc_line.webm)

## Define placement of the bump section

**Notice** : dont forget to use expressions with references to the `Spreadsheet` values.

You have two choices:
* **Solution 1**: Create constraints to place center of arc in good place based on origin of sketch and create constraints to place the end of the line :
    ![solution 1 for bump section](images/section_bump_sol1.png)
* **Solution 2**: Created a link to edges of `Pad` with button ![Sketch workbench/External geometry icon](https://wiki.freecadweb.org/images/b/b2/Sketcher_External.svg). By Selecting the edges of `Pad` this edges are "cloned" inside sketch in red color. You can apply constraints between theses external edges and your arc and line. Using for example the contraint _point on object_ with button ![Sketch workbench/Point on Object icon](https://wiki.freecadweb.org/images/6/64/Constraint_PointOnObject.svg) to align arc with the edge of the pad:
    ![solution 2 for bump section](images/section_bump_sol2.png)

The god point of solution 2 is you are easy creating implicit dependencies with the form of the part. If there will be any change in dimensions of the part (for example the height), automatically the section will replaced according the new position of references edges. But the bad point of the solution 2 is that if the changes affect topologies (example : the number of faces in pda changes) FreeCAD might not follow the right reference face.


## Create a additive pipe
1. after close sketch editor, select `Sketch001` and click on button ![Part Design/Additive Pipe Icon](https://wiki.freecadweb.org/images/2/20/PartDesign_AdditivePipe.svg)
2. in `Pipe parameters` windows in `Tasks` tab in combo view, set `Corner Transition` to `Right Corner`
3. push `Add edge` button to select an edge, then repeat operation to the other edge like this image (edges to pick in green): ![parameter of additive pipe](images/additive_pipe_parameters.png)
4. validate

Additive pipe is now fuse with the precedent pad. 

To see more information about additive pipes :
https://wiki.freecadweb.org/PartDesign_AdditivePipe

# [step 3](step3.md)

[return to step 1](step1.md)