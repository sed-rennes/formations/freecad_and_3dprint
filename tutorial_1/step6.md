# Step 6 : Duplicate channels on the brick

![step 6](images/step6.png)

In [step 3](step3/md) you learned to apply a Linear pattern to duplicate a bump. Now you will combine a polar pattern and a linear pattern together to duplicate twice the first channel with the Multi-transform tool ![PartDesign/MultiTransform](https://wiki.freecadweb.org/images/b/b7/PartDesign_MultiTransform.svg).

1. Selected `Pocket`
2. Launch `MultiTransform` tool
3. Right click in `Transformations` part of `MultiTransform parameters` windows
4. Click in contextual menu `Add polar pattern` then slide down in the windows to access to specific parameters for polar transformation :
    * Set `Axis` to `Select reference...` then select this edge (in green): ![polar axis selection](images/select_polar_axis.png)
    * Check `Reverse direction`
    * Set `Angle` to 90°
    * validate with lower `OK` button
5. Right click **again** in `Transformations` part of `MultiTransform parameters`
6. Click in contextual menu `Add linear pattern` and proceed as preview step 3 for set parameters of linear pattern in low part of the `MultiTransform parameters` windows:
    * Direction : `Base X axis`
    * Renverse Direction : true
    * Length : `Spreadsheet.base_size * 2`
    * validate with lower `OK` button
7. Validate MultiTransform

# [step 7](step7.md)

[return to step 5](step5.md)