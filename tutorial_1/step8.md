# Step 8 : Create the last channel on the brick

![step 8](images/step8.png)

In this step you can apply directly a Polar pattern with the tool ![PartDesign/Polar pattern icon](https://wiki.freecadweb.org/images/e/ef/PartDesign_PolarPattern.svg) as in [step 6](step6.md) adjusting the parameters.

# [step 9](step9.md)

[return to step 7](step7.md)