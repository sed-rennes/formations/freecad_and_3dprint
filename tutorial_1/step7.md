# Step 7 : Create a longitudinal channel on the brick

![step 7](images/step7.png)

This step is quite similar to [step 5](step5.md) just adjust face and direction to apply the pocket tool.

# [step 8](step8.md)

[return to step 6](step6.md)