# Step 9 : Create _Eyes_ of the brick

![step 9](images/step9.png)

In this step you can create a sketch based on the bottom face of the `Pad`. then apply pocket tool with `Throw all` distance.
You can use new constraints like equal length ![Sketch/Equal Icon](https://wiki.freecadweb.org/images/b/b9/Constraint_EqualLength.svg)
You might experiment with construction mode ![Sketch/Construction mode Icon](https://wiki.freecadweb.org/images/4/4c/Sketcher_AlterConstruction.svg) that allow you to add geometries elements in sketch for help you to constraint the part but the resulting elements will not visible outside of sketch. For example, you can use a horizontal construction line to aline eyes horizontally and set to his line a distance constraint to set the distance between eyes centers.
![Eyes skecth](images/eyes_sketch.png)

# [step 10 & 11](step10.md)

[return to step 8](step8.md)